package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;

import com.sun.prism.paint.Color;

import aircraft.Flight;
import eu.hansolo.medusa.Gauge;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import reciever.Radar;

public class StackPaneController {

	private static class CustomThing {
		private String from;
		private String to;
		private String squawk;
		private String type;
		private String model;

		public String getFrom() {
			return from;
		}

		public String getTo() {
			return to;
		}

		public String getSquawk() {
			return squawk;
		}

		public String getType() {
			return type;
		}

		public String getModel() {
			return model;
		}

		public CustomThing(String from, String to, String squawk, String type, String model) {
			super();
			this.from = from;
			this.to = to;
			this.squawk = squawk;
			this.type = type;
			this.model = model;
		}
	}

	@FXML
	private ListView<CustomThing> flightsList;
	@FXML
	private TextField callsignField;

	@FXML
	private TextField fromField;
	@FXML
	private TextField toField;
	@FXML
	private TextField squawkField;
	@FXML
	private TextField typeField;
	@FXML
	private TextField icaoField;
	@FXML
	private TextField modelField;
	@FXML
	private Gauge speedGauge;
	@FXML
	private Gauge verticalSpeedGauge;
	@FXML
	private Gauge altitudeGauge;



	

	public static JSONArray test = new JSONArray();
	List<Flight> loty = new ArrayList<Flight>();
	ObservableList<CustomThing> data = FXCollections.observableArrayList();
	

	@FXML
	void initialize() throws Exception {
		
		 
		 
			final Radar radar = new Radar();

			
			  Thread thread = new Thread(new Runnable() {

		            public void run() {
		                Runnable updater = new Runnable() {

		                    public void run() {

		                    
		                        data.clear();
		                        flightsList.setItems(data);
		                        for(int i = 0; i < loty.size(); i++) {
									
									data.addAll(new CustomThing(loty.get(i).getDirectionFrom(), loty.get(i).getDirectionTo(), loty.get(i).getAircraftSquawk(),loty.get(i).getAircraftModel(), loty.get(i).getAircraftType()));
									}
		                      
		                    }
		                };
		                int i = 0;
		                while (true) {
		                 	try {
		                 		
								loty = radar.aggregationRadarInfo();
							} catch (Exception e) {

								System.out.println("Virtual Radar connection error");
							}
	                        try {
	                        	if(i == 0) {
	                        		
	                        	}else {
	                        		Thread.sleep(10000);	
	                        	}
								
							} catch (InterruptedException e) {

								e.printStackTrace();
							}

		                    Platform.runLater(updater);
		                    
		                    i++;
		                }
		            }

		        });
			  
			  thread.start();
			
	        flightsList.setCellFactory(new Callback<ListView<CustomThing>, ListCell<CustomThing>>() {
	        	public ListCell<CustomThing> call(ListView<CustomThing> flightsList) {
	                return new CustomListCell();
	            }
	        });

	        
	    
	        
	        flightsList.setItems(data);
	     	
	        

	        EventHandler<MouseEvent> handler = new EventHandler<MouseEvent>(){
	        
	        	String selectedAircraft;
	        	
	        	
	        	public void handle(MouseEvent event) {
	        		
	        		try {
	        		selectedAircraft = flightsList.getSelectionModel().getSelectedItem().getSquawk();
	        		
	        		for(int i = 0; i < loty.size(); i++) {
	        		
	        			
	    				String callsign = loty.get(i).getAircraftCallsign();
	    				String from = loty.get(i).getDirectionFrom();
	    				String to = loty.get(i).getDirectionTo();
	    				String squawk = loty.get(i).getAircraftSquawk();
	    				String type = loty.get(i).getAircraftType();
	    				String icao = loty.get(i).getAircraftSquawk();
	    				String model = loty.get(i).getAircraftModel();
	    				double speed = loty.get(i).getAircraftSpeed();
	    				double verticalSpeed = loty.get(i).getAircraftVerticalSpeed();
	    				double altitude = loty.get(i).getAircraftAltitude();
	    				double signal = loty.get(i).getAircraftSignal();
	    				if(selectedAircraft.equals(squawk)) {
	    					callsignField.setText(callsign);
	    					fromField.setText(from);
	    					toField.setText(to);
	    					squawkField.setText(squawk);
	    					typeField.setText(type);
	    					icaoField.setText(icao);
	    					modelField.setText(model);
	    					speedGauge.setValue(speed * 1.609344);
	    					verticalSpeedGauge.setValue(verticalSpeed * 0.3048);
	    					altitudeGauge.setValue(altitude * 0.3048);
	    			
	    					

	    				}
	    			}
	        		}catch(NullPointerException e){
	        			System.out.println("Not Found");
	        		}
	        	}
	        };
	        
	        flightsList.addEventHandler(MouseEvent.MOUSE_CLICKED, handler);
	        
	       
	        

	}

	private class CustomListCell extends ListCell<CustomThing> {
		private HBox content = new HBox();
		private Label from;
		private Label to;
		private Label squawk;
		private Label model;
		private Pane pane;

		public CustomListCell() {
			super();
			from = new Label("");
			to = new Label("");
			squawk = new Label("");
			model = new Label("");
			pane = new Pane();
			
			
			content.setMinHeight(40.0);
			content.setPadding(new Insets(10, 10, 10, 10)); 
			
			

			content.getChildren().addAll(from, to, squawk,model);
			content.setHgrow(pane, Priority.ALWAYS);
			

		}

		@Override
		protected void updateItem(CustomThing item, boolean empty) {
			super.updateItem(item, empty);
			String checkTo = "";
			String checkAircraft = "";
			
			if (item != null && !empty) { // <== test for null item and empty parameter	
				
				to.setText(item.getTo());
				checkTo = item.getTo();
				
				if(!checkTo.equals("") && !checkTo.equals("EPPO Poznań-�?awica, Poznań, Poland")) {
					to.setText("To: " + item.getTo() + ", ");
					content.setStyle("-fx-background-color: #56b6ff;");
				}else if(!checkTo.equals("") && checkTo.equals("EPPO Poznań-�?awica, Poznań, Poland")){
					content.setStyle("-fx-background-color: #34ce29;");
					
				}
				else {
					to.setText("To: Unknow, ");
					
					content.setStyle("-fx-background-color: #8b8d91;");
				}
				model.setText(item.getType());
				checkAircraft = item.getType();
				if(!checkAircraft.equals("")) {
					model.setText("Aircraft: "+ item.getType());
				}else {
					model.setText("Aircraft: Unknow");
				}
				setGraphic(content);

			} else {
				setGraphic(null);
			}
		}
	}

}
