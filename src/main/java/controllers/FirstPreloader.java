package controllers;

import java.io.File;



import javafx.application.Preloader;
import javafx.application.Preloader.StateChangeNotification.Type;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
public class FirstPreloader extends Preloader {
private Stage preloaderStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
       this.preloaderStage = primaryStage;
 
       FXMLLoader loader = new FXMLLoader();
       loader.setLocation(this.getClass().getResource("../theme/initialScreen.fxml"));
 
       StackPane stackPane = loader.load();

       Scene scene = new Scene(stackPane);

//       To add Tower connection should add localization path for your VLC Player and EPPO2.PLS
//       ProcessBuilder pb = new ProcessBuilder(PATH_TO_VLC_PLAYER, PATH_TO_EPPO2.PLS);
//       Process start =  pb.start() ;
       Thread.sleep(1000);
       primaryStage.setScene(scene);
       primaryStage.show();
       
      

     
   }
 
   @Override
   public void handleStateChangeNotification(StateChangeNotification stateChangeNotification) {
      if (stateChangeNotification.getType() == Type.BEFORE_START) {
    	  try {
			Thread.sleep(3000);

		} catch (InterruptedException e) {

			e.printStackTrace();
		}
         preloaderStage.hide();
      }
   }
}