package aircraft;

public class Flight {
	
	private String DirectionFrom = "";
	private String DirectionTo = "";
	private String AircraftCallsign = "";
	private String AircraftType = "";
	private String AircraftLongtitude = "";
	private String AircraftLatitude = "";
	private String AircraftSquawk = "";
	private String AircraftModel = "";
	private Double AircraftSpeed = 0.0;
	private Double AircraftVerticalSpeed = 0.0;
	private Double AircraftAltitude = 0.0;
	private Double AircraftSignalLevel = 0.0;
	
	public Flight(String directionFrom, String directionTo, String aircraftCallsign, String aircraftType, String aircraftLatitude, String aircraftLongtitude, String aircraftSquawk, String aircraftModel, double aircraftSpeed, double aircraftVerticalSpeed, double aircraftAltitude, double aircraftSignal) {
		DirectionFrom = directionFrom;
		DirectionTo = directionTo;
		AircraftCallsign = aircraftCallsign;
		AircraftType = aircraftType;
		AircraftLongtitude = aircraftLongtitude;
		AircraftLatitude = aircraftLatitude;
		AircraftSquawk = aircraftSquawk;
		AircraftModel = aircraftModel;
		AircraftSpeed = aircraftSpeed;
		AircraftVerticalSpeed = aircraftVerticalSpeed;
		AircraftAltitude = aircraftAltitude;
		AircraftSignalLevel = aircraftSignal;
		
		
	}
	
	
	public String getDirectionFrom() {
		return DirectionFrom;
	}
	public String getDirectionTo() {
		return DirectionTo;
	}
	public String getAircraftCallsign() {
		return AircraftCallsign;
	}
	public String getAircraftType() {
		return AircraftType;
	}
	public String getAircraftLongtitude() {
		return AircraftLongtitude;
	}
	public String getAircraftLatitude() {
		return AircraftLatitude;
	}
	public String getAircraftSquawk() {
		return AircraftSquawk;
	}
	public String getAircraftModel() {
		return AircraftModel;
	}
	public double getAircraftSpeed() {
		return AircraftSpeed;
	}
	public double getAircraftVerticalSpeed() {
		return AircraftVerticalSpeed;
	}
	public double getAircraftAltitude() {
		return AircraftAltitude;
	}
	public double getAircraftSignal() {
		return AircraftSignalLevel;
	}


	
	

}
