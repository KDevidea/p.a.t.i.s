package main;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

 

public class App extends Application {



    @Override
    public void start(Stage primaryStage) throws IOException {

        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("../theme/patis.fxml"));
        StackPane stackPane = loader.load();
       

        Scene scene = new Scene(stackPane);     
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }



}
 