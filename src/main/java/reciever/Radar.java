package reciever;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import aircraft.Flight;




public class Radar {
	
	public JSONArray radarJSON = new JSONArray();

	public JSONArray getRadarInfo() throws Exception {
		String url = "https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?lat=52.3967476&lng=16.926047700000026&fDstL=0&fDstU=100";
		String USER_AGENT = "Mozilla/5.0";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		con.setRequestProperty("User-Agent", USER_AGENT);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
	
		String jObject = response.toString();
		JSONObject virtualRadarObject = (JSONObject) new JSONTokener(jObject).nextValue();
		JSONArray virtualRadar = (JSONArray) virtualRadarObject.get("acList");
		
		radarJSON = virtualRadar;
		return radarJSON;
		

	}
	
	
	public void setRadarData() throws Exception {
		
		JSONArray radarData = new JSONArray();
		radarData = this.getRadarInfo();
		
		radarJSON = radarData;
		
		
		
	}
	
	
	public List<Flight> aggregationRadarInfo() throws Exception {
		
		JSONArray flightsInfo = new JSONArray();
		flightsInfo = this.getRadarInfo();
		
		
		
		
		List<Flight> Flights = new ArrayList<Flight>();
		int currentsFlightsCount = flightsInfo.length();
		
		JSONObject singleFlight = new JSONObject();
		
		for(int i = 0; i < currentsFlightsCount; i++) {
			singleFlight = flightsInfo.getJSONObject(i);
			String directionFrom = singleFlight.optString("From");
			String directionTo = singleFlight.optString("To");
			String aircraftCallsign = singleFlight.optString("Call");
			String aircraftType = singleFlight.optString("Op");
			String aircraftLatitude = singleFlight.optString("Lat");
			String aircraftLongtitude = singleFlight.optString("Long");
			String aircraftSquawk = singleFlight.optString("Sqk");
			String aircraftModel = singleFlight.optString("Mdl");
			double aircraftSpeed = singleFlight.optDouble("Spd");
			double aircraftVerticalSpeed = singleFlight.optDouble("Vsi");
			double aircraftAltitude = singleFlight.optDouble("Alt");
			double aircraftSignalLevel = singleFlight.optDouble("Sig");
			
			
			Flights.add(new Flight(directionFrom, directionTo, aircraftCallsign, aircraftType, aircraftLatitude, aircraftLongtitude, aircraftSquawk, aircraftModel, aircraftSpeed, aircraftVerticalSpeed, 
					aircraftAltitude, aircraftSignalLevel));
			
		}
		
		
		return Flights;
		
		
		
		
	}
	
	
	
	
	  

}
